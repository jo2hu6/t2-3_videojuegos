using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KunaiScript : MonoBehaviour
{
    public float velocityX = 20f;
    private Rigidbody2D rb;
    private NinjaController ninjaController;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        ninjaController = FindObjectOfType<NinjaController>();
        Destroy(gameObject,3);
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = Vector2.right * velocityX;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.collider.tag == "Enemy")
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
            ninjaController.IncrementarPuntajeEn10();  
        }
    }
}
