using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NinjaController : MonoBehaviour
{
    private SpriteRenderer sr;
    private Animator _animator;
    private Rigidbody2D rb2d;
    private BoxCollider2D col;

    private bool puedeSaltar = false;
    private bool puedeSubirEscalera = false;
    private int speedUp2 = 20;

    public Text lifeText;
    public Text scoreText;
    private int Score = 0;
    private int Life = 3;

    private float alturaMax;
    private float altura;
    private Vector3 posActual;
    private bool resetAltura = false;

    public GameObject kunaiRight;
    public GameObject kunaiLeft;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        col = GetComponent<BoxCollider2D>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

        lifeText.text = "VIDA: " + Life;
        scoreText.text = "PUNTAJE: " + Score;

        //DESPLAZARSE
        if (Input.GetKey(KeyCode.RightArrow))
        {
            sr.flipX= false;
            setRunAnimation();
            rb2d.velocity = new Vector2(15,rb2d.velocity.y);
        }
        else
        {
            setIdleAnimation();
            rb2d.velocity = new Vector2(0,rb2d.velocity.y);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            sr.flipX = true;
            setRunAnimation();
            rb2d.velocity = new Vector2(-15,rb2d.velocity.y);
        }

        //SALTAR
        if (Input.GetKeyDown(KeyCode.Space) && puedeSaltar)
        {
            setJumpAnimation();
            float speedUp = 35;
            rb2d.velocity = Vector2.up * speedUp;
            puedeSaltar = false;
        }

        //SUBIR ESCALERA
        if (Input.GetKeyDown(KeyCode.UpArrow) && puedeSubirEscalera)
        {
            setIdleAnimation();
            //col.enabled = false;
            rb2d.gravityScale = 0;
            rb2d.velocity = Vector2.up * speedUp2;
            DeshabilitarColisionConSuelo();
        }

        //BAJAR ESCALERA Y DESLIZARSE
        if (Input.GetKey(KeyCode.DownArrow))
        {
            if(puedeSubirEscalera)
            {
                setIdleAnimation();
                rb2d.gravityScale = 10;
                //col.enabled = true;
                rb2d.velocity = Vector2.down * speedUp2;
                DeshabilitarColisionConSuelo();
            }

            if(puedeSubirEscalera == false)
            {
                setSlideAnimation();
            }
        }

        if(puedeSaltar == false)
        {
            posActual = transform.position;
	        altura = posActual.y;
	        if(alturaMax <= altura){
		        alturaMax = posActual.y;
	        }
	        if(resetAltura){
		        ResetAlturaMax();
	        }
            Debug.Log("Altura: "+alturaMax.ToString());
            
        }

        //PLANEAR
        if(Input.GetKey(KeyCode.F) && puedeSaltar == false)
        {
            setGlideAnimation();
            rb2d.gravityScale = 1;
            alturaMax = 0;
        }

        //DISPARAR
        if(Input.GetKeyDown(KeyCode.S))
        {
            if(!sr.flipX)
            {
                var position = new Vector2(transform.position.x + 1, transform.position.y);
                Instantiate(kunaiRight, position, kunaiRight.transform.rotation);
            }
            else
            {
                var position = new Vector2(transform.position.x - 2, transform.position.y);
                Instantiate(kunaiLeft, position, kunaiLeft.transform.rotation);
            }
        }

        //MORIR
        if(Life == 0)
        {
            setDeadAnimation();
            StartCoroutine("EndGame");
        }

    }

    private void ResetAlturaMax () {
        alturaMax = altura;
        resetAltura = false;
    }


    IEnumerator EndGame()
    {
        yield return new WaitForSeconds(1f);
        UnityEngine.Debug.Break();
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.layer == 3 || other.gameObject.layer == 7){
            puedeSaltar = true;
            rb2d.gravityScale = 8;
        }
        if(other.gameObject.tag == "Enemy")
        {
            DisminuirPuntajeEn1();
        }
        if(alturaMax >= 20 && other.gameObject.layer == 3)
        {
            Life = 0;
        }
    }

    void OnTriggerStay2D(Collider2D other) {
        if(other.gameObject.tag == "Stair")
        {
            puedeSubirEscalera = true;
            rb2d.gravityScale = 0;
            //rb2d.velocity = new Vector2(0,0);
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.tag == "Stair")
        {
            puedeSubirEscalera = false;
            rb2d.gravityScale = 10;
            HabilitarColisionConSuelo();
        }
    }

    public void DisminuirPuntajeEn1()
    {
        Life -= 1;
    }

    public void IncrementarPuntajeEn10()
    {
        Score += 10;
    }

    public void DeshabilitarColisionConSuelo()
    {
        Physics2D.IgnoreLayerCollision(6,7,true);
    }

    public void HabilitarColisionConSuelo()
    {
        Physics2D.IgnoreLayerCollision(6,7,false);
    }

    private void setIdleAnimation(){
        _animator.SetInteger("State",0);
    }

    private void setRunAnimation(){
       _animator.SetInteger("State",1);
    }

    private void setJumpAnimation(){
        _animator.SetInteger("State",2);
    }

    private void setSlideAnimation(){
        _animator.SetInteger("State",3);
    }
    
    private void setThrowAnimation(){
        _animator.SetInteger("State",4);
    }

    private void setGlideAnimation(){
        _animator.SetInteger("State",5);
    }
    
    private void setDeadAnimation(){
        _animator.SetInteger("State",6);
    }

}
