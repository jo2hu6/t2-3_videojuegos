using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieScript : MonoBehaviour
{
    private Rigidbody2D rb;
    public GameObject eg;
    public float speed;

    // Start is called before the first frame update

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        InvokeRepeating("GenerarEnemigo", 3, 7);
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = Vector2.left * speed;
        Physics2D.IgnoreLayerCollision(7,8,true);
    }

    private void GenerarEnemigo()
    {
        var x = eg.transform.position.x;
        var y = eg.transform.position.y;
        Rigidbody2D instance = Instantiate(rb, new Vector2(x,y), rb.transform.rotation);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.name == "EnemyClear") Destroy(this.gameObject);
    }

}
